package masternode;

import bywise.jsonmodel.StarterOrder;
import bywise.jsonmodel.HeaderBlock;
import bywise.jsonmodel.Block;
import bywise.Wallet;
import bywise.dao.AccountDAO;
import bywise.dao.BlockDAO;
import bywise.dao.BlockListDAO;
import bywise.dao.MempoolDAO;
import bywise.exception.AccountNotFoundException;
import bywise.exception.FormaterJsonException;
import bywise.exception.IdOperationRepetedException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.jsonmodel.Operation;
import bywise.jsonmodel.SlowOrder;
import bywise.model.AccountModel;
import bywise.model.BlockListModel;
import bywise.model.BlockModel;
import bywise.model.MempoolModel;
import helper.Helper;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MasterNode {

    public static void main(String[] args) throws Exception {
        Wallet master = Wallet.loadWallet(new File("master.wlt"));
        Wallet wallet = Wallet.loadWallet(new File("wallet.wlt"));

        SlowOrder so = SlowOrder.generateSlowOrder(master, wallet.getPublicKeyString(), 10240);
        so.validate();
        System.out.println(so);
 /*SlowOrder so = Helper.GSON.fromJson(jsonSO, SlowOrder.class);
        so.validate();
        MempoolDAO.getInstance().persist(MempoolModel.getModel(so));*/
        //System.out.println(Helper.convertWise(0.25d));
        //genetateMasterWallet();
        //generateBlockZero();
        //testBlockZero();
        //addBlocks(generateBlock(master), true);
    }

    public static void testBlockZero() throws JSONException, FormaterJsonException, AccountNotFoundException, Exception {
        Wallet master = Wallet.loadWallet(new File("master.wlt"));
        AccountModel m = AccountDAO.getInstance().findByToken(master.getPublicToken());
        if (m == null) {
            m = AccountModel.newAccount(master.getPublicKeyString());
            AccountDAO.getInstance().persist(m);
        }
        String jsonFile = Helper.readFile((new MasterNode()).getClass().getResourceAsStream("/bywise/assets/zeroBlock.json"));
        JSONArray listBlock = new JSONArray(jsonFile);
        ArrayList<Block> blks = new ArrayList();
        for (int i = 0; i < HeaderBlock.LIMIT_BLOCKLINE; i++) {
            blks.add(Helper.GSON.fromJson(listBlock.getString(i), Block.class));
        }
        addBlocks(blks, false);
    }

    public static synchronized void addBlocks(ArrayList<Block> blks, boolean validate) throws FormaterJsonException, AccountNotFoundException {
        boolean isValid = true;
        ArrayList<BlockModel> bms = new ArrayList();
        ArrayList<AccountModel> accountsUpdated = new ArrayList(), accountsCreated = new ArrayList();
        ArrayList<BlockListModel> blms = new ArrayList();
        List<BlockModel> lastBlocks = BlockDAO.getInstance().getLastBlocks();
        //execute blocks
        for (int i = 0; i < blks.size() && isValid; i++) {
            Block blk = blks.get(i);

            //Validate idBlock
            BlockModel lastBlock = null;
            for (int j = 0; j < lastBlocks.size(); j++) {
                BlockModel get = lastBlocks.get(j);
                if (get.blockLine == i) {
                    lastBlock = get;
                }
            }
            long lastIdBlock = -1;
            if (lastBlock != null) {
                lastIdBlock = lastBlock.idBlock;
            }
            if (lastIdBlock + 1 != blk.head.idBlock) {
                throw new FormaterJsonException("idBlock is " + blk.head.idBlock + " but last idBlock is " + lastIdBlock);
            }
            //Validation Opetarations
            for (int j = 0; j < blk.head.size && isValid; j++) {
                Operation op = blk.getOperation(j);
                if (validate) {
                    op.validate();
                }
                try {
                    BlockListModel blm = BlockListModel.getModel(op);
                    BlockListDAO.getInstance().isUnique(blm);
                    blms.add(blm);
                    op.applyOperation(accountsCreated, accountsUpdated);
                } catch (InsufficientFundsException | InvalidValueException | IdOperationRepetedException | InvalidPublicTokenException ex) {
                    isValid = false;
                    Logger.getLogger(MasterNode.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //Validate Block
            try {
                if (validate) {
                    blk.validate();
                }
                blk.applyOperation(accountsCreated, accountsUpdated);
                bms.add(BlockModel.getModel(blk));
            } catch (InvalidPublicTokenException | InsufficientFundsException | InvalidValueException ex) {
                isValid = false;
                Logger.getLogger(MasterNode.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (i % 100 == 0) {
                System.out.println("Tested block " + blk.head.idBlock + ", " + blk.head.blockLine);
            }
        }
        if (isValid) { //save changes
            for (int i = blms.size() - 1; i >= 0; i--) {
                BlockListModel blm = blms.get(i);
                boolean repeted = false;
                for (int j = 0; j < blms.size() && !repeted; j++) {
                    if (blms.get(j).operationUniqueToken.equals(blm.operationUniqueToken) && i != j) {
                        repeted = true;
                    }
                }
                if (repeted) {
                    blms.remove(i);
                }
            }
            BlockDAO.getInstance().persist(bms);
            AccountDAO.getInstance().merge(accountsUpdated);
            BlockListDAO.getInstance().persist(blms);
            AccountDAO.getInstance().persist(accountsCreated);
        }
    }

    public static ArrayList<Block> generateBlock(Wallet wallet) throws Exception {
        List<BlockModel> lastBlocks = BlockDAO.getInstance().getLastBlocks();
        ArrayList<Block> blks = new ArrayList();
        for (int i = 0; i < HeaderBlock.LIMIT_BLOCKLINE; i++) {
            List<MempoolModel> mempool = MempoolDAO.getInstance().getAll(i);
            ArrayList<Operation> ops = new ArrayList();
            for (int j = 0; j < mempool.size(); j++) {
                ops.add(MempoolModel.getModel(mempool.get(j)));
            }
            BlockModel lastBlock = null;
            for (int j = 0; j < lastBlocks.size(); j++) {
                BlockModel get = lastBlocks.get(j);
                if (get.blockLine == i) {
                    lastBlock = get;
                }
            }
            Block blk = new Block(ops);
            blk.head.blockDate = 1559358000; //2019-06-01 00:00
            blk.head.blockLine = i;
            blk.head.idBlock = lastBlock.idBlock + 1;
            blk.head.lastBlockHash = lastBlock.blockHash;
            blk.head.tokenCreator = wallet.getPublicToken();
            Object[] operationsTmp = blk.operations;
            blk.operations = null;
            blk.head.blockHash = blk.getHash();
            blk.head.hashSortedOperation = BlockListDAO.getInstance().getSortedOperation(blk.head.blockHash, i);
            blk.head.signatureCreator = Base64.getEncoder().encodeToString(wallet.sign(blk.getBytes()));
            blk.operations = operationsTmp;
            blks.add(blk);
            if (i % 100 == 0) {
                System.out.println("Generate block " + blk.head.idBlock + ", " + blk.head.blockLine);
            }
        }
        return blks;
    }

    public static void generateBlockZero() throws Exception {
        Wallet master = Wallet.loadWallet(new File("master.wlt"));
        //System.out.println("Master Key => " + master.getPublicKeyString());
        //System.out.println("Master Token => " + master.getPublicToken());
        AccountModel m = AccountModel.newAccount(master.getPublicKeyString());
        AccountDAO.getInstance().persist(m);
        ArrayList<AccountModel> accountsUpdated = new ArrayList(), accountsCreated = new ArrayList();
        JSONArray masterBlockZero = new JSONArray();
        for (int i = 0; i < HeaderBlock.LIMIT_BLOCKLINE; i++) {
            Block blk;
            if (i == 0) {
                StarterOrder so = StarterOrder.generateStarterOrder();
                BlockListModel blm = new BlockListModel();
                blm.idBlock = 0L;
                blm.blockLine = 0;
                blm.positionBlock = 0L;
                blm.tokenCredited = so.head.tokenCredited;
                blm.tokenDebited = so.head.tokenDebited;
                blm.idOperation = so.head.idOperation;
                blm.typeOperation = so.head.typeOperation;
                blm.hashOperation = so.head.hashOperation;
                blm.jsonOperation = so.toString();
                BlockListDAO.getInstance().persist(blm);
                so.applyOperation(accountsCreated, accountsUpdated);
                blk = new Block(Arrays.asList(so));
            } else {
                blk = new Block();
            }
            blk.head.blockDate = 1559358000; //2019-06-01 00:00
            blk.head.blockLine = i;
            blk.head.idBlock = 0;
            blk.head.lastBlockHash = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
            blk.head.tokenCreator = master.getPublicToken();
            Object[] operationsTmp = blk.operations;
            blk.operations = null;
            blk.head.blockHash = blk.getHash();
            blk.head.hashSortedOperation = BlockListDAO.getInstance().getSortedOperation(blk.head.blockHash, i);
            blk.head.signatureCreator = Base64.getEncoder().encodeToString(master.sign(blk.getBytes()));
            blk.operations = operationsTmp;
            blk.validate();
            masterBlockZero.put(new JSONObject(blk.toString()));
        }
        Helper.saveFile(new File("src/bywise/assets/zeroBlock.json"), masterBlockZero.toString());
    }

    public static void genetateMasterWallet() throws Exception {
        Wallet w;
        int blockLine;
        do {
            w = Wallet.generateWallet();
            byte[] hash = Helper.hash(w.getPublicKey());
            blockLine = Helper.bytesToInt(new byte[]{hash[0], hash[1], hash[2], hash[3]});
        } while (blockLine != 0);

        AccountModel a = AccountModel.newAccount(w.getPublicKeyString());
        System.out.println(a.publicToken);
        System.out.println(a.blockLine);
        Wallet.saveWallet(w, new File("data/master.wlt"));
    }
}
