package bywise.model;

public enum StateBlockEnum {
    TESTING(0),
    VALIDATED(1),
    CONSOLIDATED(2);
    
    private final int stateOperation;

    StateBlockEnum(int stateOperation) {
        this.stateOperation = stateOperation;
    }

    public int getState() {
        return stateOperation;
    }
}
