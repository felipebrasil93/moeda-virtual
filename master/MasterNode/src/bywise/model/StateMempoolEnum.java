package bywise.model;

public enum StateMempoolEnum {
    INVALIDATED(-1),
    VALIDATED(1),
    UNTESTED(0),
    VALIDATED_INBLOCK(2),
    INVALIDATED_INBLOCK(-2);
    
    private final int stateOperation;

    StateMempoolEnum(int stateOperation) {
        this.stateOperation = stateOperation;
    }

    public int getState() {
        return stateOperation;
    }
}
