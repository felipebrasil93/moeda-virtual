package bywise.model;

import bywise.jsonmodel.Header;
import bywise.jsonmodel.Operation;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Cacheable(false)

public class MempoolModel implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(length = 8)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idMempool;
    @Column(length = 54, unique = true)
    public String operationUniqueToken = "";
    //Order information
    @Column(length = 44)
    public String tokenDebited = "";
    @Column(length = 44)
    public String tokenCredited = "";
    @Column(length = 4)
    public Integer blockLineDebited = 0;
    @Column(length = 4)
    public Integer blockLineCredited = 0;
    @Column(length = 4)
    public Integer idOperation = 0;
    @Column(length = 4)
    public Integer typeOperation = -1;
    @Column(length = 44)
    public String hashOperation = "";
    @Column(length = 4)
    public Integer state = StateMempoolEnum.VALIDATED.getState();
    @Column(length = 2300)
    public String jsonOperation = "";

    public Long getIdMempool() {
        return idMempool;
    }

    public void setIdMempool(Long idMempool) {
        this.idMempool = idMempool;
    }
    
    public static MempoolModel getModel(Operation op){
        MempoolModel mm = new MempoolModel();
        mm.blockLineCredited = helper.Helper.getBlockLine(op.head.tokenCredited);
        mm.blockLineDebited = helper.Helper.getBlockLine(op.head.tokenDebited);
        mm.tokenDebited = op.head.tokenDebited;
        mm.tokenCredited = op.head.tokenCredited;
        mm.idOperation = op.head.idOperation;
        mm.typeOperation = op.head.typeOperation;
        mm.hashOperation = op.head.hashOperation;
        mm.jsonOperation = op.getMiniJson();
        return mm;
    }
    
    public static Operation getModel(MempoolModel mm){
        Operation op = Operation.getOperationByJson(mm.typeOperation, mm.jsonOperation);
        op.head = new Header();
        op.head.tokenDebited = mm.tokenDebited;
        op.head.tokenCredited = mm.tokenCredited;
        op.head.idOperation = mm.idOperation;
        op.head.typeOperation = mm.typeOperation;
        op.head.hashOperation = mm.hashOperation;
        return op;
    }
    
    public MempoolModel getClone() {
        try {
            return (MempoolModel) clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(MempoolModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
