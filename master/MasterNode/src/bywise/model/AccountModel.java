package bywise.model;

import bywise.dao.AccountDAO;
import bywise.exception.AccountNotFoundException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.jsonmodel.HeaderBlock;
import helper.Helper;
import java.io.Serializable;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AccountModel implements Serializable, Cloneable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 8)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAccount;
    @Column(length = 4)
    public Integer blockLine = 0;
    @Column(length = 44, unique = true)
    public String publicToken;
    @Column(length = 294)
    public String publicKey;
    @Column(length = 8)
    public Long wise = 0L;
    @Column(length = 8)
    public Long wiseConsolidated = 0L;
    @Column(length = 8)
    public Long validations = 0L;

    public Long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    private AccountModel() {
    }

    private AccountModel(String publicKey) {
        this.publicKey = publicKey;
        byte[] bytes = Base64.getDecoder().decode(publicKey);
        byte[] hash = Helper.hash(bytes);
        this.publicToken = Base64.getEncoder().encodeToString(hash);
        this.blockLine = Helper.getBlockLine(hash);
    }

    public void addWise(long value) throws InsufficientFundsException {
        if (wise + value >= 0) {
            wise += value;
        } else {
            throw new InsufficientFundsException("Account " + publicToken + " have " + wise + " but value is " + value);
        }
    }

    public static AccountModel newAccount(String publicKey) throws InvalidPublicTokenException {
        AccountModel newAC = new AccountModel(publicKey);
        AccountModel oldAC = AccountDAO.getInstance().findByToken(newAC.publicToken);
        if (oldAC == null) {
            return newAC;
        } else {
            if (oldAC.publicKey.equals(newAC.publicKey)) {
                return newAC;
            } else {
                throw new InvalidPublicTokenException("Token is repeted " + newAC.publicKey);
            }
        }
    }
    
    public AccountModel getClone() {
        try {
            return (AccountModel) clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(AccountModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
