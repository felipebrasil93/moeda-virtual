package bywise.model;

import bywise.jsonmodel.Block;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BlockModel implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(length = 8)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long idBlockSequence;
    //Blockchain identification
    @Column(length = 8)
    public Long idBlock = 0L;
    @Column(length = 4)
    public Integer blockLine = 0;
    @Column(length = 8)
    public Long blockDate = 0L;
    //Blockchain information
    @Column(length = 4)
    public Integer size = 1;
    @Column(length = 44)
    public String lastBlockHash = "";
    @Column(length = 44)
    public String tokenCreator = "";
    @Column(length = 44)
    public String hashSortedOperation = "";
    @Column(length = 344)
    public String signatureCreator = "";
    @Column(length = 44)
    public String blockHash = "";

    public Long getBlock() {
        return idBlock;
    }

    public void setBlock(Long idBlock) {
        this.idBlock = idBlock;
    }

    public Long getIdBlockSequence() {
        return idBlockSequence;
    }

    public void setIdBlockSequence(Long idBlockSequence) {
        this.idBlockSequence = idBlockSequence;
    }
    
    public static BlockModel getModel(Block blk){
        BlockModel bm = new BlockModel();
        bm.blockDate = blk.head.blockDate;
        bm.blockHash = blk.head.blockHash;
        bm.blockLine = blk.head.blockLine;
        bm.idBlock = blk.head.idBlock;
        bm.hashSortedOperation = blk.head.hashSortedOperation;
        bm.lastBlockHash = blk.head.lastBlockHash;
        bm.signatureCreator = blk.head.signatureCreator;
        bm.size = blk.head.size;
        bm.tokenCreator = blk.head.tokenCreator;
        return bm;
    }
    
    public static Block getModel(BlockModel bm){
        Block blk = new Block();
        blk.head.blockDate = bm.blockDate;
        blk.head.blockHash = bm.blockHash;
        blk.head.blockLine = bm.blockLine;
        blk.head.idBlock = bm.idBlock;
        blk.head.lastBlockHash = bm.lastBlockHash;
        blk.head.signatureCreator = bm.signatureCreator;
        blk.head.size = bm.size;
        blk.head.tokenCreator = bm.tokenCreator;
        return blk;
    }
    
    public BlockModel getClone() {
        try {
            return (BlockModel) clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(BlockModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
