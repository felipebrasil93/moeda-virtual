package bywise.model;

import bywise.jsonmodel.Operation;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BlockListModel implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(length = 8)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBlockList;
    @Column(length = 54, unique = true)
    public String operationUniqueToken = "";
    //Block information
    @Column(length = 8)
    public Long idBlock = 0L;
    @Column(length = 4)
    public Integer blockLine = 0;
    @Column(length = 8)
    public Long positionBlock = 0L;
    @Column(length = 3)
    public Integer state = StateBlockEnum.TESTING.getState();
    //Header
    @Column(length = 44)
    public String tokenDebited = "";
    @Column(length = 44)
    public String tokenCredited = "";
    @Column(length = 4)
    public Integer idOperation = 0;
    @Column(length = 4)
    public Integer typeOperation = 0;
    @Column(length = 44)
    public String hashOperation = "";
    //Operation
    @Column(length = 2300)
    public String jsonOperation = "";

    public Long getIdBlockList() {
        return idBlockList;
    }

    public void setIdBlockList(Long idBlockList) {
        this.idBlockList = idBlockList;
    }
    
    public static BlockListModel getModel(Operation op){
        BlockListModel blm = new BlockListModel();
        blm.operationUniqueToken = op.head.tokenDebited+op.head.idOperation;
        blm.tokenDebited = op.head.tokenDebited;
        blm.tokenCredited = op.head.tokenCredited;
        blm.idOperation = op.head.idOperation;
        blm.typeOperation = op.head.typeOperation;
        blm.hashOperation = op.head.hashOperation;
        blm.jsonOperation = op.getMiniJson();
        return blm;
    }
    
    public static Operation getModel(BlockListModel blm){
        Operation op = Operation.getOperationByJson(blm.typeOperation, blm.jsonOperation);
        op.head.tokenDebited = blm.tokenDebited;
        op.head.tokenCredited = blm.tokenCredited;
        op.head.idOperation = blm.idOperation;
        op.head.typeOperation = blm.typeOperation;
        op.head.hashOperation = blm.hashOperation;
        return op;
    }
    
    public BlockListModel getClone() {
        try {
            return (BlockListModel) clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(BlockListModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
