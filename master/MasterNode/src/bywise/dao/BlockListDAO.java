package bywise.dao;

import bywise.exception.IdOperationRepetedException;
import bywise.model.BlockListModel;
import bywise.model.StateBlockEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class BlockListDAO implements Serializable {

    private static BlockListDAO instance;
    private EntityManager entityManager;

    public static BlockListDAO getInstance() {
        if (instance == null) {
            instance = new BlockListDAO();
        }
        return instance;
    }

    private BlockListDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bywiseNodePU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public void persist(BlockListModel obj) throws IdOperationRepetedException {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(obj);
                entityManager.getTransaction().commit();
            } catch (Exception ex) {
                throw new IdOperationRepetedException("Id operation by " + obj.tokenDebited + " is repeted; idOperation = " + obj.idOperation);
            }
        }
    }

    public BlockListModel getById(final long id) {
        synchronized (LiteSQLDatabase.class) {
            BlockListModel obj = entityManager.find(BlockListModel.class, id).getClone();
            if(obj!=null){
                return obj.getClone();
            } else {
                return null;
            }
        }
    }

    public void isUnique(BlockListModel obj) throws IdOperationRepetedException {
        if (findOperationUniqueToken(obj.operationUniqueToken) != null) {
            throw new IdOperationRepetedException("Operation " + obj.tokenDebited + " is repeted; idOperation = " + obj.idOperation);
        }
    }

    @SuppressWarnings("unchecked")
    public BlockListModel findOperationUniqueToken(String operationUniqueToken) {
        synchronized (LiteSQLDatabase.class) {
            List<BlockListModel> list = entityManager.createNativeQuery("SELECT * FROM " + BlockListModel.class.getSimpleName() + " WHERE operationUniqueToken = '" + operationUniqueToken + "' LIMIT 1;", BlockListModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public String getSortedOperation(String blockHash, int blockLine) {
        int count;
        int limit = 10;
        List<BlockListModel> list;
        String substr;
        synchronized (LiteSQLDatabase.class) {
            do {
                substr = blockHash.substring(0, limit);
                list = searchFor(substr, blockLine);
                count = 0;
                for (BlockListModel blm : list) {
                    if (blm.hashOperation.startsWith(substr)) {
                        count++;
                    }
                }
                limit--;
            } while (count == 0 && limit != 0);
        }
        for (BlockListModel blm : list) {
            if (blm.hashOperation.startsWith(substr)) {
                return blm.hashOperation;
            }
        }
        return "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
    }

    private List<BlockListModel> searchFor(String s, int blockLine) {
        List<BlockListModel> list = entityManager.createNativeQuery("SELECT * FROM " + BlockListModel.class.getSimpleName() + " WHERE (blockLine == '" + blockLine + "') AND hashOperation LIKE '" + s + "%' ORDER BY idBlock DESC, positionBlock ASC;", BlockListModel.class).getResultList();
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).getClone());
        }
        return list;
    }

    public boolean merge(BlockListModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.merge(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }

    public void persist(ArrayList<BlockListModel> objs) {
        synchronized (LiteSQLDatabase.class) {
            int lastID = (int) entityManager.createNativeQuery("SELECT SEQ_COUNT FROM SEQUENCE WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").getSingleResult();
            do {
                ArrayList<BlockListModel> tmpList = new ArrayList();
                for (int i = 0; i < objs.size() && i < 300; i++) {
                    tmpList.add(objs.get(i));
                }
                String sql = "INSERT INTO BLOCKLISTMODEL (IDBLOCKLIST,BLOCKLINE,HASHOPERATION,IDBLOCK, IDOPERATION,JSONOPERATION,OPERATIONUNIQUETOKEN,POSITIONBLOCK,STATE,TOKENCREDITED,TOKENDEBITED,TYPEOPERATION) VALUES ";
                for (int i = 0; i < tmpList.size(); i++) {
                    BlockListModel get = tmpList.get(i);
                    lastID++;
                    sql += "(" + lastID + "," + get.blockLine + ",'" + get.hashOperation + "'," + get.idBlock + "," + get.idOperation + ",'" + get.jsonOperation + "','" + get.operationUniqueToken + "'," + get.positionBlock + "," + get.state + ",'" + get.tokenCredited + "','" + get.tokenDebited + "'," + get.typeOperation + ")";
                    if (i >= tmpList.size() - 1) {
                        sql += ";";
                    } else {
                        sql += ",";
                    }
                    objs.remove(get);
                }
                if (!tmpList.isEmpty()) {
                    entityManager.getTransaction().begin();
                    entityManager.createNativeQuery(sql).executeUpdate();
                    entityManager.getTransaction().commit();
                }
            } while (!objs.isEmpty());
            entityManager.getTransaction().begin();
            entityManager.createNativeQuery("UPDATE SEQUENCE SET SEQ_COUNT = " + lastID + " WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").executeUpdate();
            entityManager.getTransaction().commit();
        }
    }

    public boolean removeById(final int id) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                BlockListModel obj = entityManager.find(BlockListModel.class, id);
                entityManager.remove(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }
}
