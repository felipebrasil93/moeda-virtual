package bywise.dao;

import bywise.exception.IdOperationRepetedException;
import bywise.model.MempoolModel;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MempoolDAO implements Serializable {

    private static MempoolDAO instance;
    private EntityManager entityManager;

    public static MempoolDAO getInstance() {
        if (instance == null) {
            instance = new MempoolDAO();
        }
        return instance;
    }

    private MempoolDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bywiseNodePU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public void persist(MempoolModel obj) throws IdOperationRepetedException {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(obj);
                entityManager.getTransaction().commit();
            } catch (Exception ex) {
                throw new IdOperationRepetedException("Id operation by " + obj.tokenDebited + " is repeted; idOperation = " + obj.idOperation);
            }
        }
    }

    public MempoolModel getById(final long id) {
        synchronized (LiteSQLDatabase.class) {
            MempoolModel obj = entityManager.find(MempoolModel.class, id).getClone();
            if(obj!=null){
                return obj.getClone();
            } else {
                return null;
            }
        }
    }
    
    public void isUnique(MempoolModel obj) throws IdOperationRepetedException {
        if (findOperationUniqueToken(obj.operationUniqueToken) != null) {
            throw new IdOperationRepetedException("Operation " + obj.tokenDebited + " is repeted; idOperation = " + obj.idOperation);
        }
    }

    @SuppressWarnings("unchecked")
    public MempoolModel findOperationUniqueToken(String operationUniqueToken) {
        synchronized (LiteSQLDatabase.class) {
            List<MempoolModel> list = entityManager.createNativeQuery("SELECT * FROM " + MempoolModel.class.getSimpleName() + " WHERE operationUniqueToken = '" + operationUniqueToken + "' LIMIT 1;", MempoolModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0);
            }
        }
    }

    public List<MempoolModel> getAll(int blockLine) {
        synchronized (LiteSQLDatabase.class) {
            List<MempoolModel> list = entityManager.createNativeQuery("SELECT * FROM " + MempoolModel.class.getSimpleName() + " WHERE blockLineDebited == '" + blockLine + "' OR blockLineCredited == '" + blockLine + "';", MempoolModel.class).getResultList();
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i).getClone());
            }
            return list;
        }
    }

    public boolean merge(MempoolModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.merge(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }

    public boolean removeById(final int id) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                MempoolModel obj = entityManager.find(MempoolModel.class, id);
                entityManager.remove(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }
}
