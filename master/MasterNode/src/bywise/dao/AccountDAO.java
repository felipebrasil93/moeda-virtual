package bywise.dao;

import bywise.model.AccountModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AccountDAO {

    private static AccountDAO instance;
    private EntityManager entityManager;

    public static AccountDAO getInstance() {
        if (instance == null) {
            instance = new AccountDAO();
        }
        return instance;
    }

    private AccountDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bywiseNodePU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public boolean persist(AccountModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                return false;
            }
        }
    }
    
    public void persist(ArrayList<AccountModel> objs) {
        objs.forEach((obj) -> {
            System.out.println(obj.publicToken);
        });
        synchronized (LiteSQLDatabase.class) {
            int lastID = (int) entityManager.createNativeQuery("SELECT SEQ_COUNT FROM SEQUENCE WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").getSingleResult();
            do {
                ArrayList<AccountModel> tmpList = new ArrayList();
                for (int i = 0; i < objs.size() && i < 500; i++) {
                    tmpList.add(objs.get(i));
                }
                String sql = "INSERT INTO ACCOUNTMODEL (IDACCOUNT,BLOCKLINE,PUBLICKEY,PUBLICTOKEN,VALIDATIONS,WISE,WISECONSOLIDATED) VALUES ";
                for (int i = 0; i < tmpList.size(); i++) {
                    AccountModel get = tmpList.get(i);
                    lastID++;
                    sql += "(" + lastID + "," + get.blockLine + ",'" + get.publicKey + "','" + get.publicToken + "'," + get.validations + "," + get.wise + "," + get.wiseConsolidated + ")";
                    if (i >= tmpList.size() - 1) {
                        sql += ";";
                    } else {
                        sql += ",";
                    }
                    objs.remove(get);
                }
                if (!tmpList.isEmpty()) {
                    entityManager.getTransaction().begin();
                    entityManager.createNativeQuery(sql).executeUpdate();
                    entityManager.getTransaction().commit();
                }
            } while (!objs.isEmpty());
            entityManager.getTransaction().begin();
            entityManager.createNativeQuery("UPDATE SEQUENCE SET SEQ_COUNT = "+lastID+" WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").executeUpdate();
            entityManager.getTransaction().commit();
        }
    }

    @SuppressWarnings("unchecked")
    public AccountModel findByToken(String publicToken) {
        synchronized (LiteSQLDatabase.class) {
            List<AccountModel> list = entityManager.createNativeQuery("SELECT * FROM " + AccountModel.class.getSimpleName() + " WHERE publicToken = '" + publicToken + "' LIMIT 1;", AccountModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0).getClone();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public AccountModel findByKey(String publicKey) {
        synchronized (LiteSQLDatabase.class) {
            List<AccountModel> list = entityManager.createNativeQuery("SELECT * FROM " + AccountModel.class.getSimpleName() + " WHERE publicKey = '" + publicKey + "' LIMIT 1;", AccountModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0).getClone();
            }
        }
    }

    private boolean merge(AccountModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.merge(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }
    
    public void merge(ArrayList<AccountModel> objs) {
        synchronized (LiteSQLDatabase.class) {
            do {
                ArrayList<AccountModel> tmpList = new ArrayList();
                for (int i = 0; i < objs.size() && i < 500; i++) {
                    tmpList.add(objs.get(i));
                }
                String sql = "";
                for (int i = 0; i < tmpList.size(); i++) {
                    AccountModel get = tmpList.get(i);
                    sql += "UPDATE ACCOUNTMODEL SET VALIDATIONS="+get.validations+",WISE="+get.wise+",WISECONSOLIDATED="+get.wiseConsolidated+" WHERE IDACCOUNT="+get.getIdAccount()+";";
                    objs.remove(get);
                }
                if (!tmpList.isEmpty()) {
                    entityManager.getTransaction().begin();
                    entityManager.createNativeQuery(sql).executeUpdate();
                    entityManager.getTransaction().commit();
                }
            } while (!objs.isEmpty());
        }
    }
}
