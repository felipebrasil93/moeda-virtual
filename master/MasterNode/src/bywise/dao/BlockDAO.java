package bywise.dao;

import bywise.jsonmodel.HeaderBlock;
import bywise.model.BlockModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class BlockDAO implements Serializable {

    private static BlockDAO instance;
    private EntityManager entityManager;

    public boolean persist(BlockModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.persist(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }

    public void persist(ArrayList<BlockModel> objs) {
        synchronized (LiteSQLDatabase.class) {
            int lastID = (int) entityManager.createNativeQuery("SELECT SEQ_COUNT FROM SEQUENCE WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").getSingleResult();
            do {
                ArrayList<BlockModel> tmpList = new ArrayList();
                for (int i = 0; i < objs.size() && i < 500; i++) {
                    tmpList.add(objs.get(i));
                }
                String sql = "INSERT INTO BLOCKMODEL (IDBLOCKSEQUENCE,IDBLOCK,BLOCKDATE,BLOCKHASH,BLOCKLINE,LASTBLOCKHASH,SIGNATURECREATOR,HASHSORTEDOPERATION,SIZE,TOKENCREATOR) VALUES ";
                for (int i = 0; i < tmpList.size(); i++) {
                    BlockModel get = tmpList.get(i);
                    lastID++;
                    sql += "(" + lastID + "," + get.idBlock + "," + get.blockDate + ",'" + get.blockHash + "'," + get.blockLine + ",'" + get.lastBlockHash + "','" + get.signatureCreator + "','" + get.hashSortedOperation + "'," + get.size + ",'" + get.tokenCreator + "')";
                    if (i >= tmpList.size() - 1) {
                        sql += ";";
                    } else {
                        sql += ",";
                    }
                    objs.remove(get);
                }
                if (!tmpList.isEmpty()) {
                    entityManager.getTransaction().begin();
                    entityManager.createNativeQuery(sql).executeUpdate();
                    entityManager.getTransaction().commit();
                }
            } while (!objs.isEmpty());
            entityManager.getTransaction().begin();
            entityManager.createNativeQuery("UPDATE SEQUENCE SET SEQ_COUNT = "+lastID+" WHERE SEQ_NAME = 'SEQ_GEN_IDENTITY';").executeUpdate();
            entityManager.getTransaction().commit();
        }
    }

    public static BlockDAO getInstance() {
        if (instance == null) {
            instance = new BlockDAO();
        }
        return instance;
    }

    private BlockDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bywiseNodePU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public BlockModel getById(final long id) {
        synchronized (LiteSQLDatabase.class) {
            BlockModel obj = entityManager.find(BlockModel.class, id);
            if(obj!=null){
                return obj.getClone();
            } else {
                return null;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List findAll() {
        synchronized (LiteSQLDatabase.class) {
            List<BlockModel> list = entityManager.createQuery("FROM " + BlockModel.class.getName(), BlockModel.class).getResultList();
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i).getClone());
            }
            return list;
        }
    }

    @SuppressWarnings("unchecked")
    public BlockModel findByToken(String publicToken) {
        synchronized (LiteSQLDatabase.class) {
            List<BlockModel> list = entityManager.createNativeQuery("SELECT * FROM " + BlockModel.class.getSimpleName() + " WHERE publicToken = '" + publicToken + "' LIMIT 1;", BlockModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0).getClone();
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public BlockModel getLastBlock(int blockLine) {
        synchronized (LiteSQLDatabase.class) {
            List<BlockModel> list = entityManager.createNativeQuery("SELECT * FROM " + BlockModel.class.getSimpleName() + " WHERE blockLine = '" + blockLine + "' ORDER BY IDBLOCK DESC LIMIT 1;", BlockModel.class).getResultList();
            if (list.isEmpty()) {
                return null;
            } else {
                return list.get(0).getClone();
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<BlockModel> getLastBlocks() {
        synchronized (LiteSQLDatabase.class) {
            List<BlockModel> list = entityManager.createNativeQuery("SELECT * FROM " + BlockModel.class.getSimpleName() + " ORDER BY IDBLOCK DESC LIMIT "+HeaderBlock.LIMIT_BLOCKLINE+";", BlockModel.class).getResultList();
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i).getClone());
            }
            return list;
        }
    }

    public boolean merge(BlockModel obj) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                entityManager.merge(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }

    public boolean removeById(final int id) {
        synchronized (LiteSQLDatabase.class) {
            try {
                entityManager.getTransaction().begin();
                BlockModel obj = entityManager.find(BlockModel.class, id);
                entityManager.remove(obj);
                entityManager.getTransaction().commit();
                return true;
            } catch (Exception ex) {
                //ex.printStackTrace();
                //entityManager.getTransaction().rollback();
                return false;
            }
        }
    }
}
