package bywise.exception;

public class InvalidPublicTokenException extends Exception {

    public InvalidPublicTokenException(String string) {
        super(string);
    }
    
}
