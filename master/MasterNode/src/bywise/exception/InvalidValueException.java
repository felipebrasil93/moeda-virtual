package bywise.exception;

public class InvalidValueException extends Exception {

    public InvalidValueException(String string) {
        super(string);
    }
    
}
