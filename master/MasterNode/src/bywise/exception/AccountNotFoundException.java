package bywise.exception;

public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String string) {
        super(string);
    }
    
}
