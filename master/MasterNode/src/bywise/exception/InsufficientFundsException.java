package bywise.exception;

public class InsufficientFundsException extends Exception {

    public InsufficientFundsException(String string) {
        super(string);
    }
    
}
