package bywise.exception;

public class IdOperationRepetedException extends Exception {

    public IdOperationRepetedException(String string) {
        super(string);
    }
    
}
