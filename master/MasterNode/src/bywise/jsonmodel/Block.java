package bywise.jsonmodel;

import bywise.exception.FormaterJsonException;
import bywise.Wallet;
import bywise.dao.AccountDAO;
import bywise.dao.BlockDAO;
import bywise.exception.AccountNotFoundException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.model.AccountModel;
import bywise.model.BlockModel;
import helper.Helper;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Block extends JsonModel {

    //header
    public HeaderBlock head;
    //list orders
    public String[] tokenDebited;
    public String[] tokenCredited;
    public int[] idOperation;
    public int[] typeOperation;
    public String[] hashOperation;
    public Object[] operations;

    public Block(List<Operation> ops) {
        head = new HeaderBlock();
        if (!ops.isEmpty()) {
            tokenDebited = new String[ops.size()];
            tokenCredited = new String[ops.size()];
            idOperation = new int[ops.size()];
            typeOperation = new int[ops.size()];
            hashOperation = new String[ops.size()];
            operations = new Operation[ops.size()];
            for (int i = 0; i < ops.size(); i++) {
                Operation o = ops.get(i);
                tokenDebited[i] = o.head.tokenDebited;
                tokenCredited[i] = o.head.tokenCredited;
                idOperation[i] = o.head.idOperation;
                typeOperation[i] = o.head.typeOperation;
                hashOperation[i] = o.head.hashOperation;
                operations[i] = o.getCloneWithoutHeader();
            }
            head.size = ops.size();
        }
    }

    public Block() {
        head = new HeaderBlock();
    }

    public Operation getOperation(int i) {
        Operation op = Operation.getOperationByJson(typeOperation[i], helper.Helper.GSON.toJson(operations[i]));
        op.head = new Header();
        op.head.tokenDebited = tokenDebited[i];
        op.head.tokenCredited = tokenCredited[i];
        op.head.idOperation = idOperation[i];
        op.head.typeOperation = typeOperation[i];
        op.head.hashOperation = hashOperation[i];
        return op;
    }

    @Override
    public void validate() throws FormaterJsonException {
        //Validate Header
        if (head == null) {
            throw new FormaterJsonException("Header is null");
        }
        head.validate();
        //Validate Nulled Body
        if (tokenDebited == null && head.size != 0) {
            throw new FormaterJsonException("tokenDebited is null");
        }
        if (tokenCredited == null && head.size != 0) {
            throw new FormaterJsonException("tokenCredited is null");
        }
        if (idOperation == null && head.size != 0) {
            throw new FormaterJsonException("idOperation is null");
        }
        if (typeOperation == null && head.size != 0) {
            throw new FormaterJsonException("typeOperation is null");
        }
        if (hashOperation == null && head.size != 0) {
            throw new FormaterJsonException("hashOperation is null");
        }
        //Validate permission
        AccountModel ac = AccountDAO.getInstance().findByToken(this.head.tokenCreator);
        if (ac == null) {
            throw new FormaterJsonException("Account creator " + this.head.tokenCreator + " not found!");
        }
        if (!ac.publicToken.equals(StarterOrder.publicTokenMaster)) {
            //Test permission (wise and validations)
        }
        //Validate hash and signature
        //  Generate hash and bytes
        Object[] operationsTmp = this.operations;
        String tmpHash = this.head.blockHash;
        String tmpSign = this.head.signatureCreator;
        String hashSortedOperation = this.head.hashSortedOperation;
        this.head.blockHash = null;
        this.head.signatureCreator = null;
        this.operations = null;
        this.head.hashSortedOperation = null;
        String hash = this.getHash();
        this.head.blockHash = tmpHash;
        this.head.hashSortedOperation = hashSortedOperation;
        byte[] bytes = this.getBytes();
        this.head.signatureCreator = tmpSign;
        this.operations = operationsTmp;
        //  Test Hash and bytes
        if (!head.blockHash.equals(hash)) {
            throw new FormaterJsonException("Unmatched hash block");
        }
        try {
            if (!Wallet.verify(bytes, Base64.getDecoder().decode(tmpSign), Base64.getDecoder().decode(ac.publicKey))) {
                throw new FormaterJsonException("Invalid signature");
            }
        } catch (Exception ex) {
            Logger.getLogger(Block.class.getName()).log(Level.SEVERE, null, ex);
            throw new FormaterJsonException("Invalid signature");
        }
        //Same size Body
        if (tokenDebited != null) {
            if (tokenDebited.length != head.size) {
                throw new FormaterJsonException("tokenDebited.length != head.size : " + tokenDebited.length + "," + head.size);
            }
            if (tokenCredited.length != head.size) {
                throw new FormaterJsonException("tokenCredited.length != head.size : " + tokenCredited.length + "," + head.size);
            }
            if (idOperation.length != head.size) {
                throw new FormaterJsonException("idOperation.length != head.size: " + idOperation.length + "," + head.size);
            }
            if (typeOperation.length != head.size) {
                throw new FormaterJsonException("typeOperation.length != head.size: " + typeOperation.length + "," + head.size);
            }
            if (hashOperation.length != head.size) {
                throw new FormaterJsonException("hashOperation.length != head.size: " + hashOperation.length + "," + head.size);
            }
            if (operations != null) {
                if (operations.length != head.size) {
                    throw new FormaterJsonException("operations.length != head.size: " + operations.length + "," + head.size);
                }
            }
            //body validation
            for (int i = 0; i < tokenDebited.length; i++) {
                validateBase64Hash(tokenDebited[i], "tokenDebited[" + i + "]");
                validateBase64Hash(tokenCredited[i], "tokenCredited[" + i + "]");
                if (idOperation[i] <= 0) {
                    throw new FormaterJsonException("idOperation must be greater than zero: idOperation[" + i + "]=" + idOperation[i]);
                }
                if (typeOperation[i] == 0) {
                    if (head.idBlock != 0 || head.blockLine != 0 || head.size != 1) {
                        throw new FormaterJsonException("typeOperation invalid: typeOperation[" + i + "]=" + typeOperation[i]);
                    }
                } else if (typeOperation[i] < 1 || typeOperation[i] > 2) {
                    throw new FormaterJsonException("typeOperation invalid: typeOperation[" + i + "]=" + typeOperation[i]);
                }
                validateBase64Hash(hashOperation[i], "hashOperation[" + i + "]");
            }
        }
    }

    public void applyOperation(ArrayList<AccountModel> acCreated, ArrayList<AccountModel> acUpdated) throws InvalidPublicTokenException, InsufficientFundsException, InvalidValueException, AccountNotFoundException {
        if (head.idBlock < 2200000) {
            AccountModel creator = null;
            for (AccountModel ac : acUpdated) {
                if (ac.publicToken.equals(head.tokenCreator)) {
                    creator = ac;
                }
            }
            for (AccountModel ac : acCreated) {
                if (ac.publicToken.equals(head.tokenCreator)) {
                    creator = ac;
                }
            }
            if (creator == null) {
                creator = AccountDAO.getInstance().findByToken(head.tokenCreator);
                if (creator != null) {
                    acUpdated.add(creator);
                } else {
                    throw new AccountNotFoundException("Token not found " + head.tokenCreator);
                }
            }
            creator.addWise(250000000L);
        }
    }

    public static Block generateBlock(Block lastBlock, Wallet creator, List<Operation> ops) throws Exception {
        Block bc = new Block(ops);
        bc.head.blockLine = lastBlock.head.blockLine;
        bc.head.idBlock = lastBlock.head.idBlock + 1;
        bc.head.blockDate = Helper.getTime();
        bc.head.size = ops.size();
        bc.head.lastBlockHash = lastBlock.head.lastBlockHash;
        bc.head.tokenCreator = creator.getPublicToken();
        bc.head.signatureCreator = Base64.getEncoder().encodeToString(creator.sign(bc.toString().getBytes()));
        bc.head.blockHash = Helper.stringHash(bc.toString().getBytes());
        return bc;
    }
}
