package bywise.jsonmodel;

import bywise.exception.FormaterJsonException;

public class FastOrder extends Order {
    //First signature
    public String signatureValidator;
    public String tokenValidator;
    //Second signature
    public String signatureDebited;
    //Third signature
    public String signatureMaster1;
    public String tokenMaster1;
    //Fourth signature
    public String signatureMaster2;
    public String tokenMaster2;

    @Override
    protected void validateJsonOrder() throws FormaterJsonException {
        validateBase64Hash(tokenValidator, "tokenValidator");
        validateBase64Hash(tokenMaster1, "tokenMaster1");
        validateBase64Hash(tokenMaster2, "tokenMaster2");
        validateSignature(signatureValidator, "signatureValidator");
        validateSignature(signatureDebited, "signatureDebited");
        validateSignature(signatureMaster1, "signatureMaster1");
        validateSignature(signatureMaster2, "signatureMaster2");
    }

    @Override
    public int getOperationType() {
        return OperationTypeEnum.FastOrder.getTypeOperation();
    }
}
