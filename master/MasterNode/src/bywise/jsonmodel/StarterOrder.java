package bywise.jsonmodel;

import bywise.dao.AccountDAO;
import bywise.exception.AccountNotFoundException;
import bywise.exception.FormaterJsonException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.model.AccountModel;
import helper.Helper;
import java.util.ArrayList;

public class StarterOrder extends Operation {

    public static final String publicKeyMasterString = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1y/qigJSZImsK+iNCNxBZ9B72uIo0PL0mIcTa+ofQqOxS3ShuIGwyivWB84fVQtKDuzRgo/wNvQdjIuXhPrN00nhIpletm+kXgPFrdEGc+I4L0RqeUBjcTrAQc5e7qDAXd+6Ohzb4mMXcIi59Ntc8qzYeAvIz9IRzvQSbyP25FYbyuz2Y5fVAhJGvu2+yNhxvJsBxGm0D5/l44OxZ0IltcWvRx5x+lTepFnbGBcOqtM3YIcwjOpOVZ/esSqk5YzpQo/kt0T4b0ATQV6sDEBX1OmmBSYn8ap6eBn0gZWNeBUIdbv8eLPDr33893LLLDH21cc7zVx2uDMmXD/LPYSd0QIDAQAB";
    public static final String publicTokenMaster = "FOpAAGI8naENS8ZdGDnxDCDqDPF2bLJVI4DJhXkxrFc=";
    public static final long starterWise = 88800000000000000L;

    public String key;
    public long total;

    private StarterOrder() {
    }

    @Override
    public void validateJsonWithoutHeader() throws FormaterJsonException {
        validateKeyOrToken(key, "key");
        validateValue(total, "total");
        if (total != starterWise) {
            throw new FormaterJsonException("invalid value");
        }
        if (!key.equals(publicKeyMasterString)) {
            throw new FormaterJsonException("invalid tokenDebited");
        }

    }

    @Override
    public void validate() throws FormaterJsonException {
        if (head == null) {
            throw new FormaterJsonException("head is null");
        }
        head.validate();
        if (head.idOperation != 1) {
            throw new FormaterJsonException("invalid idOperation");
        }
        if (head.typeOperation != OperationTypeEnum.StarterOrder.getTypeOperation()) {
            throw new FormaterJsonException("invalid typeOperation");
        }
        if (!head.tokenDebited.equals("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=")) {
            throw new FormaterJsonException("invalid tokenDebited");
        }
        if (!head.tokenCredited.equals(publicTokenMaster)) {
            throw new FormaterJsonException("invalid tokenCredited");
        }
        StarterOrder so = Helper.GSON.fromJson(this.toString(), StarterOrder.class);
        so.head.hashOperation = null;
        if (!head.hashOperation.equals(so.getHash())) {
            throw new FormaterJsonException("invalid hashOperation");
        }
        validateJsonWithoutHeader();
    }

    @Override
    public int getOperationType() {
        return OperationTypeEnum.StarterOrder.getTypeOperation();
    }

    public static StarterOrder generateStarterOrder() {
        StarterOrder so = new StarterOrder();
        so.head = new Header();
        so.head.idOperation = 1;
        so.head.tokenCredited = publicTokenMaster;
        so.head.tokenDebited = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=";
        so.head.typeOperation = OperationTypeEnum.StarterOrder.getTypeOperation();
        so.key = publicKeyMasterString;
        so.total = starterWise;
        so.head.hashOperation = so.getHash();
        return so;
    }

    @Override
    public void applyOperation(ArrayList<AccountModel> acCreated, ArrayList<AccountModel> acUpdated) throws InsufficientFundsException, InvalidValueException, InvalidPublicTokenException, AccountNotFoundException {
        AccountModel credited = null;
        for (AccountModel ac : acCreated) {
            if (ac.publicToken.equals(head.tokenCredited)) {
                credited = ac;
            }
        }
        for (AccountModel ac : acUpdated) {
            if (ac.publicToken.equals(head.tokenCredited)) {
                credited = ac;
            }
        }

        if (credited == null) {
            credited = AccountDAO.getInstance().findByToken(head.tokenCredited);
            if (credited == null) {
                credited = AccountModel.newAccount(key);
                acCreated.add(credited);
            } else {
                acUpdated.add(credited);
            }
        }
        credited.addWise(total);
    }
}
