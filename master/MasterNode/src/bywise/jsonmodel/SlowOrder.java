package bywise.jsonmodel;

import bywise.Wallet;
import bywise.dao.AccountDAO;
import bywise.exception.FormaterJsonException;
import bywise.model.AccountModel;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SlowOrder extends Order {

    public static final float DEFAULT_RATE = 0.02f;

    public String signatureDebited;

    @Override
    protected void validateJsonOrder() throws FormaterJsonException {
        validateSignature(signatureDebited, "signatureDebited");
        if (tokenOrKeyCredited.length() == 44) {
            if (!head.tokenCredited.equals(tokenOrKeyCredited)) {
                throw new FormaterJsonException("tokenCredited != tokenOrKeyCredited");
            }
        } else {
            if (!head.tokenCredited.equals(helper.Helper.stringHash(Base64.getDecoder().decode(tokenOrKeyCredited)))) {
                throw new FormaterJsonException("tokenCredited != tokenOrKeyCredited");
            }
        }
        if (head.typeOperation != OperationTypeEnum.SlowOrder.getTypeOperation()) {
            throw new FormaterJsonException("invalid typeOperation");
        }
        if (rate != (long) (value * DEFAULT_RATE)) {
            throw new FormaterJsonException("inválid rate: "+rate);
        }
        if (total != rate+value) {
            throw new FormaterJsonException("inválid total: "+total);
        }
        //validate signature
        byte[] public_key = null;
        AccountModel account = AccountDAO.getInstance().findByToken(head.tokenDebited);
        if (account != null) {
            public_key = Base64.getDecoder().decode(account.publicKey);
        } else {
            throw new FormaterJsonException("inválid tokenDebited: "+head.tokenDebited);
        }
        String sign = signatureDebited;
        signatureDebited = null;
        byte[] data = getBytes();
        signatureDebited = sign;
        try {
            if (!Wallet.verify(data, Base64.getDecoder().decode(signatureDebited), public_key)) {
                throw new FormaterJsonException("inválid signature");
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | SignatureException ex) {
            Logger.getLogger(SlowOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getOperationType() {
        return OperationTypeEnum.SlowOrder.getTypeOperation();
    }
    
    public static SlowOrder generateSlowOrder(Wallet wallet, String tokenOrKeyCredited, double value) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        return generateSlowOrder(wallet, tokenOrKeyCredited, null, (new Random()).nextInt(1000000000), value);
    }
    
    public static SlowOrder generateSlowOrder(Wallet wallet, String tokenOrKeyCredited, String reason, double value) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        return generateSlowOrder(wallet, tokenOrKeyCredited, reason, (new Random()).nextInt(1000000000), value);
    }
    
    public static SlowOrder generateSlowOrder(Wallet wallet, String tokenOrKeyCredited, int idOperation, double value) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        return generateSlowOrder(wallet, tokenOrKeyCredited, null, idOperation, value);
    }

    public static SlowOrder generateSlowOrder(Wallet wallet, String tokenOrKeyCredited, String reason, int idOperation, double value) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        SlowOrder so = new SlowOrder();
        so.date = helper.Helper.getTime();
        so.value = helper.Helper.convertWise(value);
        so.rate = (long) (so.value * DEFAULT_RATE);
        so.total = so.value + so.rate;
        so.reason = reason;
        so.tokenOrKeyCredited = tokenOrKeyCredited;
        so.head = new Header();
        so.head.idOperation = idOperation;
        if (tokenOrKeyCredited.length() == 44) {
            so.head.tokenCredited = tokenOrKeyCredited;
        } else {
            so.head.tokenCredited = helper.Helper.stringHash(Base64.getDecoder().decode(tokenOrKeyCredited));
        }
        so.head.tokenDebited = wallet.getPublicToken();
        so.head.typeOperation = OperationTypeEnum.SlowOrder.getTypeOperation();
        so.head.hashOperation = so.getHash();
        so.signatureDebited = Base64.getEncoder().encodeToString(wallet.sign(so.getBytes()));
        return so;
    }
}
