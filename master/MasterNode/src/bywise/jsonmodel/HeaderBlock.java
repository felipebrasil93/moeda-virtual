package bywise.jsonmodel;

import bywise.dao.BlockListDAO;
import bywise.exception.FormaterJsonException;

public class HeaderBlock implements Validator {
    public static final int LIMIT_BLOCKLINE = 0x1000;
    
    public long idBlock;//id do bloco na linha de bloco
    public int blockLine; //linda de bloco
    public long blockDate; //data do bloco em Era UNIK
    public int size; //tamanho bloco
    public String lastBlockHash;
    public String tokenCreator;
    public String hashSortedOperation;
    public String signatureCreator;
    public String blockHash;

    @Override
    public void validate() throws FormaterJsonException {
        if (blockLine >= LIMIT_BLOCKLINE || blockLine < 0) {
            throw new FormaterJsonException("invalid blockLine: " + blockLine);
        }
        if (idBlock < 0) {
            throw new FormaterJsonException("invalid idBlock: " + idBlock);
        }
        validateValue(blockDate, "blockDate");
        if (size >= 10000 || size < 0) {
            throw new FormaterJsonException("invalid size: " + size);
        }
        validateBase64Hash(lastBlockHash, "lastBlockHash");
        validateBase64Hash(tokenCreator, "tokenCreator");
        validateSignature(signatureCreator, "signatureCreator");
        validateBase64Hash(hashSortedOperation, "hashSortedOperation");
        validateBase64Hash(blockHash, "blockHash");
        if(!BlockListDAO.getInstance().getSortedOperation(blockHash, blockLine).equals(hashSortedOperation)){
            throw new FormaterJsonException("invalid hashSortedOperation: " + hashSortedOperation);
        }
    }
}
