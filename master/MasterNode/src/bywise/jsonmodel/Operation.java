package bywise.jsonmodel;

import bywise.exception.AccountNotFoundException;
import bywise.exception.FormaterJsonException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.model.AccountModel;
import java.util.ArrayList;

public abstract class Operation extends JsonModel {

    public Header head; //header for the transaction

    public abstract int getOperationType();

    public abstract void applyOperation(ArrayList<AccountModel> acCreated, ArrayList<AccountModel> acUpdated) throws InsufficientFundsException, InvalidValueException, InvalidPublicTokenException, AccountNotFoundException ;

    public abstract void validateJsonWithoutHeader() throws FormaterJsonException;

    public String getMiniJson() {
        Header tmpHead = this.head;
        this.head = null;
        String miniJson = this.toString();
        this.head = tmpHead;
        return miniJson;
    }

    public Operation getCloneWithoutHeader() {
        String json = this.getMiniJson();
        return helper.Helper.GSON.fromJson(json, this.getClass());
    }

    public static Operation getOperationByJson(int typeOperation, String json) {
        return (Operation) helper.Helper.GSON.fromJson(json, OperationTypeEnum.getTypeOperation(typeOperation).getTypeClass());
    }
}
