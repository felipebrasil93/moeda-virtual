package bywise.jsonmodel;

public enum OperationTypeEnum {
    StarterOrder(0, bywise.jsonmodel.StarterOrder.class),
    SlowOrder(1, bywise.jsonmodel.SlowOrder.class),
    FastOrder(2, bywise.jsonmodel.FastOrder.class);
    
    private final int typeOperation;
    private final Class typeClass;

    private OperationTypeEnum(int typeOperation, Class typeClass) {
        this.typeOperation = typeOperation;
        this.typeClass = typeClass;
    }

    public int getTypeOperation() {
        return typeOperation;
    }

    public Class getTypeClass() {
        return typeClass;
    }
    
    public static OperationTypeEnum getTypeOperation(int typeOperation){
        switch(typeOperation){
            case 0:
                return StarterOrder;
            case 1:
                return SlowOrder;
            case 2:
                return FastOrder;
            default:
                return null;
        }
    }
}
