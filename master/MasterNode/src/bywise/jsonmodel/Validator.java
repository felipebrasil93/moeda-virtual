package bywise.jsonmodel;

import bywise.exception.FormaterJsonException;

public interface Validator {
    
    public void validate() throws FormaterJsonException;
    
    default void validateJson(String data, String field, int min, int max) throws FormaterJsonException {
        if (data == null) {
            throw new FormaterJsonException("null " + field);
        } else if (data.length() < min || data.length() >= max) {
            throw new FormaterJsonException("invalid size " + field+"; field.length >= "+min+" && field.length < "+max+", "+field+".length == "+data.length());
        } else {
            if(!data.matches("^[a-zA-Z0-9/+={}\":,]*$")){
                throw new FormaterJsonException("invalid chars "+field+": "+data);
            }
        }
    }
    
    default void validateBase64Hash(String token, String field) throws FormaterJsonException {
        if (token == null) {
            throw new FormaterJsonException("null " + field);
        } else if (token.length() != 44) {
            throw new FormaterJsonException("invalid size " + field+"; token.length == 44, "+field+".length == "+token.length());
        } else {
            if(!token.matches("^[a-zA-Z0-9/+=]*$")){
                throw new FormaterJsonException("invalid chars "+field+": "+token);
            }
        }
    }
    
    default void validateKeyOrToken(String keyOrToken, String field) throws FormaterJsonException {
        if (keyOrToken == null) {
            throw new FormaterJsonException("null " + field);
        } else if (keyOrToken.length() != 44 && keyOrToken.length() != 392) {
            throw new FormaterJsonException("invalid size " + field+"; keyOrToken.length == 44 or 392, "+field+".length == "+keyOrToken.length());
        } else {
            if(!keyOrToken.matches("^[a-zA-Z0-9/+=]*$")){
                throw new FormaterJsonException("invalid chars "+field+": "+keyOrToken);
            }
        }
    }
    
    default void validateSignature(String signature, String field) throws FormaterJsonException {
        if (signature == null) {
            throw new FormaterJsonException("null " + field);
        } else if (signature.length() != 344) {
            throw new FormaterJsonException("invalid size " + field+"; signature.length == 344, "+field+".length == "+signature.length());
        } else {
            if(!signature.matches("^[a-zA-Z0-9/+=]*$")){
                throw new FormaterJsonException("invalid chars "+field+": "+signature);
            }
        }
    }
    
    default void validateValue(long value, String field) throws FormaterJsonException {
        if (value <= 0 || value >= 9100000000000000000L) {
            throw new FormaterJsonException("value invalid: "+field+"=" + value);
        }
    }
}
