package bywise.jsonmodel;

import helper.Helper;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class JsonModel implements Validator {
    
    @Override
    public String toString() {
        return Helper.GSON.toJson(this);
    }
    
    public byte[] getBytes() {
        try {
            return Helper.hash(this.toString().getBytes("ASCII"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Operation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public String getHash() {
        try {
            return Helper.stringHash(this.toString().getBytes("ASCII"));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Operation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
