package bywise.jsonmodel;

import bywise.dao.AccountDAO;
import bywise.exception.AccountNotFoundException;
import bywise.exception.FormaterJsonException;
import bywise.exception.InsufficientFundsException;
import bywise.exception.InvalidPublicTokenException;
import bywise.exception.InvalidValueException;
import bywise.model.AccountModel;
import java.util.ArrayList;

public abstract class Order extends Operation {

    public long date; //UNIX date
    public String tokenOrKeyCredited; //opitional public key credited wallet
    public String reason; //reason for the transaction
    public long rate; //value rate
    public long value; //credited value
    public long total; //total transaction value

    protected abstract void validateJsonOrder() throws FormaterJsonException;

    @Override
    public void validateJsonWithoutHeader() throws FormaterJsonException {
        validateValue(date, "date");
        validateKeyOrToken(tokenOrKeyCredited, "tokenOrKeyCredited");
        if (reason != null) {
            if (!reason.matches("^[a-zA-Z0-9\\-_\\.]{0,32}$")) {
                throw new FormaterJsonException("invalid reason: \"" + reason + "\"");
            }
        }
        validateValue(rate, "rate");
        validateValue(value, "value");
        validateValue(total, "total");

        validateJsonOrder();

        if (this.toString().length() >= 2500) {
            throw new FormaterJsonException("Order very long json: Order.toString().length() < 2500, this.length = " + this.toString().length());
        }
    }

    @Override
    public void validate() throws FormaterJsonException {
        if (head == null) {
            throw new FormaterJsonException("Header is null");
        }
        head.validate();
        validateJsonWithoutHeader();
    }
    
    @Override
    public void applyOperation(ArrayList<AccountModel> acCreated, ArrayList<AccountModel> acUpdated) throws InsufficientFundsException, InvalidValueException, InvalidPublicTokenException, AccountNotFoundException {
        AccountModel credited = null;
        for (AccountModel ac : acCreated) {
            if (ac.publicToken.equals(head.tokenCredited)) {
                credited = ac;
            }
        }
        for (AccountModel ac : acUpdated) {
            if (ac.publicToken.equals(head.tokenCredited)) {
                credited = ac;
            }
        }
        if (credited == null) {
            credited = AccountDAO.getInstance().findByToken(head.tokenCredited);
            if (credited == null) {
                if (tokenOrKeyCredited.length() == 44) {
                    throw new AccountNotFoundException("Credited token not found " + tokenOrKeyCredited);
                } else {
                    credited = AccountModel.newAccount(tokenOrKeyCredited);
                    acCreated.add(credited);
                }
            } else {
                acUpdated.add(credited);
            }
        }
        AccountModel debited = null;
        for (AccountModel ac : acUpdated) {
            if (ac.publicToken.equals(head.tokenDebited)) {
                debited = ac;
            }
        }
        for (AccountModel ac : acCreated) {
            if (ac.publicToken.equals(head.tokenDebited)) {
                debited = ac;
            }
        }
        if (debited == null) {
            debited = AccountDAO.getInstance().findByToken(head.tokenDebited);
            if (debited != null) {
                acUpdated.add(debited);
            } else {
                throw new AccountNotFoundException("Debited token not found " + head.tokenDebited);
            }
        }
        debited.addWise(-total);
        credited.addWise(value);
    }
}
