package bywise.jsonmodel;

import bywise.exception.FormaterJsonException;

public class Header implements Validator {

    public String tokenDebited; //token debited wallet
    public String tokenCredited; //token credited wallet
    public int idOperation; //Unique id of wallet operation
    public int typeOperation; //Type operation
    public String hashOperation; //hash operation with all signatures

    @Override
    public void validate() throws FormaterJsonException {
        validateBase64Hash(tokenDebited, "tokenDebited");
        validateBase64Hash(tokenCredited, "tokenCredited");
        validateBase64Hash(hashOperation, "hashOperation");
        validateBase64Hash(tokenDebited, "tokenDebited");
        if (idOperation <= 0 || idOperation > 2147483647) {
            throw new FormaterJsonException("idOperation must be greater than zero: idOperation=" + idOperation);
        }
        if (typeOperation < 0 || typeOperation > 2) {
            throw new FormaterJsonException("typeOperation invalid: typeOperation=" + typeOperation);
        }
    }
}
