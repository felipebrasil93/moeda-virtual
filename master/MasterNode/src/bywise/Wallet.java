package bywise;

import helper.Helper;
import java.io.File;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONObject;

public class Wallet {

    private boolean isCrypted = false;
    private String publicToken;
    private byte[] publicKey;
    private byte[] privateKey;
    private String publicKeyString;

    private Wallet() {
    }

    public boolean isIsCrypted() {
        return isCrypted;
    }

    public byte[] getPublicKey() {
        return publicKey;
    }

    public byte[] getPrivateKey() {
        return privateKey;
    }

    public String getPublicToken() {
        return publicToken;
    }

    public String getPublicKeyString() {
        return publicKeyString;
    }
    
    public byte[] sign(byte[] data) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        return Wallet.sign(data, privateKey);
    }

    public boolean verify(byte[] data, byte[] signature) throws Exception {
        return Wallet.verify(data, signature, publicKey);
    }

    private byte[] encrypt(byte[] data, String password) throws Exception {
        byte[] hash = Helper.hash(password.getBytes());
        SecretKey key = new SecretKeySpec(hash, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    private byte[] decrypt(byte[] cryptedData, String password) throws Exception {
        byte[] hash = Helper.hash(password.getBytes());
        Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec key = new SecretKeySpec(hash, "AES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(cryptedData);
    }

    public static Wallet generateWallet(String password) throws Exception {
        Wallet wallet = generateWallet();
        wallet.privateKey = wallet.encrypt(wallet.privateKey, password);
        wallet.isCrypted = true;
        return wallet;
    }

    public static Wallet generateWallet() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048, new SecureRandom());
        KeyPair pair = keyGen.generateKeyPair();
        
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(pair.getPublic().getEncoded());
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(pair.getPrivate().getEncoded());

        Wallet wallet = new Wallet();
        wallet.privateKey = pkcs8EncodedKeySpec.getEncoded();
        wallet.publicKey = x509EncodedKeySpec.getEncoded();
        wallet.publicKeyString = Base64.getEncoder().encodeToString(wallet.publicKey);
        wallet.publicToken = helper.Helper.stringHash(wallet.publicKey);
        return wallet;
    }

    public static byte[] sign(byte[] data, byte[] private_key) throws NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(private_key);
        PrivateKey privateKeyDecoded = keyFactory.generatePrivate(privateKeySpec);
        privateSignature.initSign(privateKeyDecoded);
        privateSignature.update(data);
        return privateSignature.sign();
    }

    public static boolean verify(byte[] data, byte[] signature, byte[] public_key) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec public_keySpec = new X509EncodedKeySpec(public_key);
        PublicKey public_keyDecoded = keyFactory.generatePublic(public_keySpec);
        publicSignature.initVerify(public_keyDecoded);
        publicSignature.update(data);
        return publicSignature.verify(signature);
    }

    public static void saveWallet(Wallet w, File f) throws Exception {
        JSONObject json = new JSONObject();
        json.put("isCrypted", w.isCrypted);
        json.put("privateKey", Base64.getEncoder().encodeToString(w.privateKey));
        json.put("publicKey", Base64.getEncoder().encodeToString(w.publicKey));
        json.put("publicToken", w.publicToken);
        helper.Helper.saveFile(f, json.toString());
    }

    public static Wallet loadWallet(File f) throws Exception {
        if (f.exists()) {
            JSONObject json = new JSONObject(helper.Helper.readFile(f));
            Wallet w = new Wallet();
            w.isCrypted = json.getBoolean("isCrypted");
            w.privateKey = Base64.getDecoder().decode(json.getString("privateKey"));
            w.publicKey = Base64.getDecoder().decode(json.getString("publicKey"));
            w.publicKeyString = json.getString("publicKey");
            w.publicToken = json.getString("publicToken");
            return w;
        }
        return null;
    }
}
