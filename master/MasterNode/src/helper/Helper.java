package helper;

import bywise.jsonmodel.HeaderBlock;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Helper {
    public static final long LONG_WISE = 1000000000L;
    
    public static Gson GSON = (new GsonBuilder()).disableHtmlEscaping().create();
    
    public static double convertWise(long wise){
        return wise/LONG_WISE;
    }
    
    public static long convertWise(double wise){
        return (long) (wise*LONG_WISE);
    }
    
    public static int getBlockLine(byte[] token){
        return Helper.bytesToInt(new byte[]{token[0], token[1], token[2], token[3]}) & (HeaderBlock.LIMIT_BLOCKLINE - 1);   // Little Endian
    }
    
    public static int getBlockLine(String token){
        byte[] bytesToken = Base64.getDecoder().decode(token);
        return Helper.bytesToInt(new byte[]{bytesToken[0], bytesToken[1], bytesToken[2], bytesToken[3]}) & (HeaderBlock.LIMIT_BLOCKLINE - 1);
    }

    public static long getTime() {
        return System.currentTimeMillis() / 1000L;
    }

    public static void saveFile(File file, String s) throws Exception {
        FileWriter arq = new FileWriter(file);
        PrintWriter gravarArq = new PrintWriter(arq);
        gravarArq.print(s);
        arq.close();
    }

    public static String readFile(File file) throws Exception {
        FileReader arq = new FileReader(file);
        BufferedReader br = new BufferedReader(arq);
        String str = "";
        while (br.ready()) {
            String linha = br.readLine();
            str += linha + "\n";
        }
        br.close();
        return str;
    }
    
    public static String readFile(InputStream file) {
        String s = "";
        InputStreamReader streamReader = new InputStreamReader(file, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        try {
            for (String line; (line = reader.readLine()) != null;) {
                s += line;
            }
        } catch (IOException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
            s = null;
        }
        return s;
    }

    public static byte[] readBinaryFile(File file) throws Exception {
        InputStream inputStream = new FileInputStream(file);
        long fileSize = file.length();
        byte[] allBytes = new byte[(int) fileSize];
        inputStream.read(allBytes);
        return allBytes;
    }

    public static byte[] hash(byte[] data) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(data, 0, data.length);
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String stringHash(byte[] data) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(data, 0, data.length);
            String hash = Base64.getEncoder().encodeToString(messageDigest.digest());
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static int bytesToInt(byte[] bytes) {
        ByteBuffer wrapped = ByteBuffer.wrap(bytes); // big-endian by default
        return wrapped.getInt();
    }

    public static byte[] intToBytes(int integer) {
        ByteBuffer dbuf = ByteBuffer.allocate(4);
        dbuf.putInt(integer);
        return dbuf.array();
    }
}
